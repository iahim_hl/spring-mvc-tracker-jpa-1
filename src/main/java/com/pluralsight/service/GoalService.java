package com.pluralsight.service;

import com.pluralsight.model.Goal;
import com.pluralsight.model.GoalReport;

import java.util.List;

/**
 * Created by evo on 11/24/2017.
 */
public interface GoalService {
    Goal saveGoal(Goal goal);

    List<Goal> findAllGoals();

    List<GoalReport> findAllGoalReports();
}
