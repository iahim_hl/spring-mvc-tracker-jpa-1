package com.pluralsight.repository;

import com.pluralsight.model.Exercise;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by evo on 11/24/2017.
 */

@Repository
public class ExerciseRepositoryImpl implements ExerciseRepository {

    @PersistenceContext
    private EntityManager em;

    public Exercise save(Exercise exercise) {
        em.persist(exercise);
        em.flush();
        return exercise;
    }
}
