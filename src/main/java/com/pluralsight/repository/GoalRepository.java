package com.pluralsight.repository;

import com.pluralsight.model.Goal;
import com.pluralsight.model.GoalReport;

import java.util.List;

/**
 * Created by evo on 11/24/2017.
 */
public interface GoalRepository {

    Goal save(Goal goal);

    List<Goal> findAll();

    List<GoalReport> findAllReports();
}
