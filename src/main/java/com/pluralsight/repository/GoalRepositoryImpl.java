package com.pluralsight.repository;

import com.pluralsight.model.Goal;
import com.pluralsight.model.GoalReport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by evo on 11/24/2017.
 */
@Repository
public class GoalRepositoryImpl implements GoalRepository {

    @PersistenceContext
    private EntityManager em;

    public Goal save(Goal goal) {

        if(goal.getId()==null) {
            em.persist(goal);
            em.flush();
        }else{
            goal = em.merge(goal);
        }
        return goal;
    }


    public List<Goal> findAll() {
//        String q = new String("Select g from Goal g");
//        Query query = em.createQuery(q);
        TypedQuery<Goal> query =
                em.createNamedQuery(Goal.FIND_ALL_GOALS, Goal.class);
        return query.getResultList();
    }

    public List<GoalReport> findAllReports() {
//        String q = new String("Select new com.pluralsight.model.GoalReport(g.minutes, e.minutes, e.activity) " +
//                "from Goal g, Exercise e where g.id = e.goal.id");
//        Query query = em.createQuery();

        TypedQuery<GoalReport> query = em.createNamedQuery(Goal.FIND_GOAL_REPORTS, GoalReport.class);
        List<GoalReport> report =query.getResultList();
        return report;
    }
}
