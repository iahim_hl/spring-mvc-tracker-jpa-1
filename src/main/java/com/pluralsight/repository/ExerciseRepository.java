package com.pluralsight.repository;

import com.pluralsight.model.Exercise;

/**
 * Created by evo on 11/24/2017.
 */
public interface ExerciseRepository {

    Exercise save(Exercise exercise);
}
