<%--
  Created by IntelliJ IDEA.
  User: evo
  Date: 11/27/2017
  Time: 10:48 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Goals Report</title>
</head>
<body>
    <table>
        <tr>
            <th>ID</th><th>Minutes</th>
            <c:forEach items="${goals}" var="goal">
                <tr>
                    <td>${goal.id}</td><td>${goal.minutes}</td>
                    <td>
                        <table>
                            <tr>
                                <th>Exercise Id </th><td>Exercise Minutes</td>
                                <c:forEach items="${goal.exercises}" var="exercise">
                                    <tr>
                                        <td>${exercise.id}</td><td>${exercise.minutes}</td><td>${exercise.activity}</td>
                                    </tr>
                                </c:forEach>
                            </tr>
                        </table>
                    </td>
                </tr>
            </c:forEach>
        </tr>
    </table>
</body>
</html>
