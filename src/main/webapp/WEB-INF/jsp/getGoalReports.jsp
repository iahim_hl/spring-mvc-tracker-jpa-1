<%--
  Created by IntelliJ IDEA.
  User: evo
  Date: 11/27/2017
  Time: 11:22 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Goal Reports</title>
</head>
<body>
<table>
    <tr>
        <th>Minutes</th><th>Exercise Minutes</th><th>Activity</th>
        <c:forEach items="${goalReports}" var="goalReport">
            <tr>
                <td>${goalReport.goalMinutes}</td><td>${goalReport.exerciseMinutes}</td><td>${goalReport.exerciseActivity}</td>
            </tr>
        </c:forEach>
    </tr>
</table>

</body>
</html>
